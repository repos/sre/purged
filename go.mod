module wikimedia.org/software/purged

go 1.17

require (
	gerrit.wikimedia.org/r/operations/software/prometheus-rdkafka-exporter v0.0.0-20221103135727-482eecb0e673
	github.com/confluentinc/confluent-kafka-go v0.11.6
	github.com/prometheus/client_golang v1.9.0
	golang.org/x/net v0.0.0-20200625001655-4c5254603344
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.15.0 // indirect
	github.com/prometheus/procfs v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20201214210602-f9fddec55a1e // indirect
	google.golang.org/protobuf v1.23.0 // indirect
)
