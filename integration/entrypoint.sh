#!/bin/bash
cd /etc && envsubst < /etc/purged-kafka.conf.tpl > /etc/purged-kafka.conf
echo "Waiting kafka to be ready on port 19092"
while ! nc -z ${MY_IP} 19092; do
    sleep 0.1
done
/usr/bin/purged -backend_addr ${MY_IP}:3128 -frontend_addr ${MY_IP}:8080 -kafkaConfig /etc/purged-kafka.conf -topics resource_change -purgeMaxAge 100
