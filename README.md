# Purged

For information and how to use this software please refer to https://wikitech.wikimedia.org/wiki/Purged

## Build

To build the binary the quickest way is to use the included `Makefile`:

```
make build
```

Refer to the various Makefile rules for other tasks.

To build the Debian package refer to [the wiki](https://wikitech.wikimedia.org/wiki/Purged#Building_the_package)

## Run

Ideally `purged` should be run by systemd, eg:

```
ExecStart=/usr/bin/purged -backend_addr 127.0.0.1:3128 -frontend_addr 127.0.0.1:3127 -prometheus_addr :2112 -frontend_workers 4 -backend_workers 96 -host_prefixes 'upload.,maps.' -topics eqiad.resource-purge,codfw.resource-purge -kafkaConfig /etc/purged/purged-kafka.conf
```

Refer to the unit (**/lib/systemd/system/purged.service**) for more information.
